//
// Created by Evgenii Ivanov on 07.01.2022.
//

#include "mem.h"
#include "mem_internals.h"
#include "testing.h"

#ifndef MAP_FIXED_NOREPLACE
#define MAP_FIXED_NOREPLACE 0x10000
#endif

// test with malloc one block
void test_1(struct block_header *heap) {
    printf("test 1 start");

    debug_heap(stdout, heap);
    printf("\nвыделяю 555: \n");
    _malloc(555);
    debug_heap(stdout, heap);
}

// test with free one block
void test_2(struct block_header *heap) {
    printf("test 2 start\n");

    void *block_to_free = _malloc(2048);
    debug_heap(stdout, heap);
    printf("\nосвобождаю 2048: \n");
    _free(block_to_free);
    debug_heap(stdout, heap);

}

// test with free of few blocks
void test_3(struct block_header *heap) {
    printf("test 3 start\n");

    void *block_to_free_1 = _malloc(100);
    void *block_to_free_2 = _malloc(200);
    void *block_to_free_3 = _malloc(300);
    void *block_to_free_4 = _malloc(400);

    debug_heap(stdout, heap);
    printf("\nосвобождение блока 3 [300]: ");
    _free(block_to_free_3);
    debug_heap(stdout, heap);
    printf("\nосвобождение блока 1 [100]: ");
    _free(block_to_free_1);
    debug_heap(stdout, heap);
    printf("\nосвобождение блока 2 [200]: ");
    _free(block_to_free_2);
    debug_heap(stdout, heap);
    printf("\nосвобождение блока 4 [400]: ");
    _free(block_to_free_4);
    debug_heap(stdout, heap);
}

// test with 'overmalloc'
void test_4(struct block_header *heap) {
    printf("test 4 start\n");

    _malloc(9000);
    debug_heap(stdout, heap);
    _malloc(34000);
    debug_heap(stdout, heap);
}

void do_nothing(void* tmp) {
    uint8_t x = 10;
    if ((uint8_t*) tmp ==  &x) {
        printf("if statement is good");
    }
    printf("do nothing start");
}

// test with new memory and moving
void test_5(struct block_header *heap) {
    printf("test 5 start\n");

    debug_heap(stdout, heap);


    struct block_header* tmp = heap;
    while (tmp->next != NULL) {
        tmp = tmp->next;
    }
    void* tmp_new_addr = mmap((void*) ((uint8_t*) tmp + size_from_capacity(tmp->capacity).bytes), 10000, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, 0, 0);
    do_nothing(tmp_new_addr);
    _malloc(10000);
    debug_heap(stdout, heap);

    printf("%s\n", "result: ");
    _malloc(4000);
    debug_heap(stdout, heap);
}


// run all tests with one heap!!!
int run_tests() {
    void* heap = heap_init(20000);
    if (heap == NULL) {
        printf("Ошибка при создании кучи.\n");
        return 1;
    }
    debug_heap(stdout, heap);

    test_1(heap);
    test_2(heap);
    test_3(heap);
    test_4(heap);
    test_5(heap);

    return 0;
}
